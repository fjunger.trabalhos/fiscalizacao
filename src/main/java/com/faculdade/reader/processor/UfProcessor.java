package com.faculdade.reader.processor;

import javax.persistence.EntityManager;

import com.faculdade.dao.UfDao;
import com.faculdade.dao.UfDaoImpl;
import com.faculdade.model.Uf;
import com.faculdade.reader.processor.csvSplitter.CsvGetterInterface;

public class UfProcessor implements Processor {

	String name = "";
	CsvGetterInterface csv;
	UfDao dao;
	private LineObjects objects;
	
	public UfProcessor(EntityManager em, LineObjects objects) {
		this.dao = new UfDaoImpl(em);
		this.objects = objects;
	}

	@Override
	public void printLine(CsvGetterInterface csv){
		System.out.println(csv.getUf());
	}

	@Override
	public void processLine(String line, CsvGetterInterface csv){
			name = csv.getUf();
			Uf uf = objects.getUf(name);
			if(uf == null ) {
				uf = createUf();
				objects.addUf(uf);
				dao.save(uf);
			}
	}

	private Uf createUf() {
		return new Uf(name,getInitials());
	}

	private String getInitials() {
		switch (name) {
		case "ACRE":
			return "AC";
		case "ALAGOAS":
			return "AL";
		case "AMAZONAS":
			return "AM";
		case "BAHIA":
			return "BA";
		case "CEARA":
			return "CE";
		case "BRASILIA":
		case "DISTRITO FEDERAL":
			return "DF";
		case "ESPIRITO SANTO":
			return "ES";
		case "GOIAS":
			return "GO";
		case "MARANHAO":
			return "MA";
		case "MATO GROSSO":
			return "MT";
		case "MATO GROSSO DO SUL":
			return "MS";
		case "MINAS GERAIS":
			return "MG";
		case "PARA":
			return "PA";
		case "PARAIBA":
			return "PB";
		case "PARANA":
			return "PR";
		case "PERNAMBUCO":
			return "PE";
		case "PIAUI":
			return "PI";
		case "RIO DE JANEIRO":
			return "RJ";
		case "RIO GRANDE DO NORTE":
			return "RN";
		case "RIO GRANDE DO SUL":
			return "RS";
		case "RONDONIA":
			return "RO";
		case "RORAIMA":
			return "RR";
		case "SANTA CATARINA":
			return "SC";
		case "SÃO PAULO":
			return "SP";
		case "SERGIPE":
			return "SE";
		case "TOCANTINS":
			return "TO";
		default:
			return "N/A";
		}
	}

}
