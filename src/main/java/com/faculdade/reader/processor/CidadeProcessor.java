package com.faculdade.reader.processor;

import javax.persistence.EntityManager;

import com.faculdade.dao.*;
import com.faculdade.model.*;
import com.faculdade.reader.processor.csvSplitter.CsvGetterInterface;

public class CidadeProcessor implements Processor {

	Uf uf;
	String name;
	CsvGetterInterface csv;

	LineObjects objects;

	CidadeDao cidadeDao;

	public CidadeProcessor(EntityManager em, LineObjects objects) {
		this.objects = objects;
		this.cidadeDao = new CidadeDaoImpl(em);
	}

	@Override
	public void printLine(CsvGetterInterface csv) {
		System.out.println(csv.getCidade());
	}

	@Override
	public void processLine(String line, CsvGetterInterface csv) {
		this.uf = objects.getUf(csv.getUf());
		Cidade cidade = objects.getCidade(csv.getCidade());
		if (cidade == null) {
			cidade = new Cidade(csv.getCidade(), uf);
			objects.addCidade(cidade);
			cidadeDao.save(cidade);
		}
	}

}
