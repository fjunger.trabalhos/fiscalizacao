package com.faculdade.reader.processor;

import java.time.LocalDate;

import javax.persistence.EntityManager;

import com.faculdade.dao.BairroDao;
import com.faculdade.dao.BairroDaoImpl;
import com.faculdade.dao.CidadeDao;
import com.faculdade.dao.CidadeDaoImpl;
import com.faculdade.dao.EmpresaDao;
import com.faculdade.dao.EmpresaDaoImpl;
import com.faculdade.dao.UfDao;
import com.faculdade.dao.UfDaoImpl;
import com.faculdade.model.Bairro;
import com.faculdade.model.Cidade;
import com.faculdade.model.Empresa;
import com.faculdade.model.Uf;
import com.faculdade.reader.processor.csvSplitter.CsvGetterInterface;

public class EmpresaProcessor implements Processor {

	Cidade cidade;
	Bairro bairro;
	Uf uf;

	CsvGetterInterface csv;

	EmpresaDao empresaDao;
//	CidadeDao cidadeDao;
//	BairroDao bairroDao;
//	UfDao ufDao;
	private LineObjects objects;

	String name;
	String cnpj;
	String logradouro;
	String cep;
	LocalDate data;

	public EmpresaProcessor(EntityManager em, LineObjects objects) {
//		this.bairroDao = new BairroDaoImpl(em);
//		this.ufDao = new UfDaoImpl(em);
//		this.cidadeDao = new CidadeDaoImpl(em);
		this.empresaDao = new EmpresaDaoImpl(em);
		this.objects = objects;
	}

	@Override
	public void printLine(CsvGetterInterface csv) {
		System.out.println(csv.getNomeEmpresa());

	}

	@Override
	public void processLine(String line, CsvGetterInterface csv) {
		this.uf = objects.getUf(csv.getUf());
		this.cidade = objects.getCidade(csv.getCidade());
		this.bairro = objects.getBairro(csv.getBairro());
		Empresa empresa = objects.getEmpresa(csv.getCnpj());
		if (empresa == null) {
			name = csv.getNomeEmpresa();
			cnpj = csv.getCnpj();
			logradouro = csv.getLogradouro();
			cep = csv.getCep();
			data = csv.getData();
			empresa = new Empresa(name, cnpj, logradouro, cep, data, cidade, bairro, uf);
			objects.addEmpresa(empresa);
			empresaDao.save(empresa);
		}

	}

}
