package com.faculdade.reader.processor;

import javax.persistence.EntityManager;

import com.faculdade.dao.BairroDao;
import com.faculdade.dao.BairroDaoImpl;
import com.faculdade.dao.CidadeDao;
import com.faculdade.dao.CidadeDaoImpl;
import com.faculdade.dao.UfDao;
import com.faculdade.dao.UfDaoImpl;
import com.faculdade.model.Bairro;
import com.faculdade.model.Cidade;
import com.faculdade.model.Uf;
import com.faculdade.reader.processor.csvSplitter.CsvGetterInterface;

public class BairroProcessor implements Processor {

	Uf uf;
	Cidade cidade;
	String name = "";
	CsvGetterInterface csv;
	LineObjects objects;
	BairroDao bairroDao;
//	UfDao ufDao;
//	CidadeDao cidadeDao;

	public BairroProcessor(EntityManager em, LineObjects objects) {
		this.bairroDao = new BairroDaoImpl(em);
//		this.ufDao = new UfDaoImpl(em);
//		this.cidadeDao = new CidadeDaoImpl(em);
		this.objects = objects;
	}

	@Override
	public void printLine(CsvGetterInterface csv) {
		System.out.println(csv.getBairro());
	}

	@Override
	public void processLine(String line, CsvGetterInterface csv) {
		name = csv.getBairro();
		this.cidade = objects.getCidade(csv.getCidade());
		this.uf = cidade.getUf();
		Bairro bairro = objects.getBairro(csv.getBairro());
		if (uf != null) {
			if (bairro == null) {
				bairro = new Bairro(name, uf, cidade);
				objects.addBairro(bairro);
				bairroDao.save(bairro);
			}
		}

	}

}
