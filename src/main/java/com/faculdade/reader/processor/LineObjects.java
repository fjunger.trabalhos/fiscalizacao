package com.faculdade.reader.processor;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.faculdade.dao.BairroDao;
import com.faculdade.dao.BairroDaoImpl;
import com.faculdade.dao.CidadeDao;
import com.faculdade.dao.CidadeDaoImpl;
import com.faculdade.dao.EmpresaDao;
import com.faculdade.dao.FiscalizacaoDao;
import com.faculdade.dao.UfDao;
import com.faculdade.dao.UfDaoImpl;
import com.faculdade.model.Bairro;
import com.faculdade.model.Cidade;
import com.faculdade.model.Empresa;
import com.faculdade.model.Fiscalizacao;
import com.faculdade.model.Uf;

public class LineObjects {

	private static UfDao daoUf;
	private static CidadeDao daoCidade;
	private static BairroDao daoBairro;
	private static EmpresaDao daoEmpresa;
	private static FiscalizacaoDao fiscalizacaoDao;
	
	private static Uf uf;
	private static Cidade cidade;
	private static Bairro bairro;
	private static Empresa empresa;
	private static Fiscalizacao fiscalizacao;
	
	private static List<Uf> ufs = new ArrayList<>();
	private static List<Cidade> cidades = new ArrayList<>();
	private static List<Bairro> bairros = new ArrayList<>();
	private static List<Empresa> empresas = new ArrayList<>();
	
	public LineObjects(EntityManager em) {
		daoUf = new UfDaoImpl(em);
		daoCidade = new CidadeDaoImpl(em);
		daoBairro = new BairroDaoImpl(em);
		populateLists();
	}
	
	private void populateLists() {
		ufs = daoUf.getAll();
		cidades = daoCidade.getAll();
		bairros = daoBairro.getAll();
		try {
			empresas = daoEmpresa.getAll();
		}catch (NullPointerException e) {
		}
	}

	protected Uf getUf(String nomeUf) {
		uf=null;
		for(Uf uf:ufs) {
			if(uf.getName().equals(nomeUf)) {
				this.uf = uf;
				break;
			}
		}
		return uf;
	}
	
	protected void addUf(Uf uf) {
		ufs.add(uf);
	}
	
	protected Cidade getCidade(String nomeCidade) {
		cidade = null;
		for(Cidade c : cidades) {
			if(c.getNome().equals(nomeCidade) && c.getUf()==uf) {
				this.cidade = c;
				break;
			}
		}
		return cidade;
	}

	protected void addCidade(Cidade cidade) {
		cidades.add(cidade);
	}

	protected Bairro getBairro(String nomeBairro) {
		bairro=null;
		for(Bairro b : bairros) {
			if(b.getName().equals(nomeBairro) && b.getCidade() == cidade) {
				this.bairro = b;
				break;
			}
		}
		return bairro;
	}
	
	protected void addBairro(Bairro bairro) {
		bairros.add(bairro);
	}
	
	protected Empresa getEmpresa(String cnpj) {
		empresa=null;
		for(Empresa e : empresas) {
			if(e.getCnpj().equals(cnpj)) {
				empresa = e;
				break;
			}
		}
		return empresa;
	}
	
	protected void addEmpresa(Empresa empresa) {
		empresas.add(empresa);
	}

	public void updateEmpresa(Empresa empresaUpdate) {
		for(Empresa e : empresas) {
			if(e.getCnpj().equals(empresaUpdate.getCnpj())) {
				empresas.remove(e);
				empresas.add(empresaUpdate);
				break;
			}
		}
	}
	
}
