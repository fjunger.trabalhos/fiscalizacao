package com.faculdade.reader.processor;


import com.faculdade.reader.processor.csvSplitter.CsvGetterInterface;

public interface Processor {

	void printLine(CsvGetterInterface csv);

	void processLine(String line, CsvGetterInterface csv);
	
}
