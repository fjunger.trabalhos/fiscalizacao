package com.faculdade.reader.processor;

import java.time.LocalDate;

import javax.persistence.EntityManager;

import com.faculdade.dao.*;
import com.faculdade.model.*;
import com.faculdade.reader.processor.csvSplitter.CsvGetterInterface;

public class FiscalizacaoProcessor implements Processor {

	Fiscalizacao fiscalizacao;
	Empresa empresa;
	Cidade cidade;
	Bairro bairro;
	Uf uf;

	CsvGetterInterface csv;

	FiscalizacaoDao fiscalizacaoDao;
	EmpresaDao empresaDao;
//	CidadeDao cidadeDao;
//	BairroDao bairroDao;
//	UfDao ufDao;

	private LineObjects objects;
	
	String name;
	String cnpj;
	String logradouro;
	String cep;
	LocalDate data;

	public FiscalizacaoProcessor(EntityManager em, LineObjects objects) {
//		this.bairroDao = new BairroDaoImpl(em);
//		this.ufDao = new UfDaoImpl(em);
//		this.cidadeDao = new CidadeDaoImpl(em);
		this.objects = objects;
		this.empresaDao = new EmpresaDaoImpl(em);
		this.fiscalizacaoDao = new FiscalizacaoDaoImpl(em);
	}

	@Override
	public void printLine(CsvGetterInterface csv) {
		System.out.println(csv.getNomeEmpresa() + " | " + csv.getData() + " | " + csv.getCidade() + " | "
				+ csv.getBairro() + " | " + csv.getUf());
	}

	@Override
	public void processLine(String line, CsvGetterInterface csv) {
		Long startTime = System.nanoTime();
		this.uf = objects.getUf(csv.getUf());
		this.cidade = objects.getCidade(csv.getCidade());
		this.bairro = objects.getBairro(csv.getBairro());
		this.empresa = objects.getEmpresa(csv.getCnpj());
		if (uf != null && bairro != null && cidade != null && empresa != null) {
			name = csv.getNomeEmpresa();
			cnpj = csv.getCnpj();
			logradouro = csv.getLogradouro();
			cep = csv.getCep();
			data = csv.getData();
			fiscalizacao = createFis();
			if (!fiscalizacaoDao.exist(fiscalizacao)) {
				fiscalizacaoDao.save(fiscalizacao);
				updateEmpresa();
			}
		}
	}

	private void updateEmpresa() {
		
		if(empresa.getLastFis().isBefore(data)) {
			empresa.setUf(uf);
			empresa.setBairro(bairro);
			empresa.setCidade(cidade);
			empresa.setCep(cep);
			empresa.setLogradouro(logradouro);
			empresa.setName(name);
			empresa.setLastFis(data);
			empresaDao.update(empresa);
			objects.updateEmpresa(empresa);
		}

	}

	private Fiscalizacao createFis() {
		return (new Fiscalizacao(name, cnpj, logradouro, cep, data, cidade, bairro, uf, empresa));
	}

}
