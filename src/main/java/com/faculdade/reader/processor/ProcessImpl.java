package com.faculdade.reader.processor;

import java.util.*;

import javax.persistence.EntityManager;

import com.faculdade.fiscalizacao.App;
import com.faculdade.reader.processor.csvSplitter.CsvGetterInterface;
import com.faculdade.reader.processor.csvSplitter.CsvGettersImpl;
import com.faculdade.reader.processor.validators.ValidateCnpj;

public class ProcessImpl implements Processor {

	private List<Processor> processors = new ArrayList<>();
	private EntityManager em;
	private LineObjects objects;

	public ProcessImpl(EntityManager em) {
		this.objects = new LineObjects(em);
		processors.add(new UfProcessor(em, objects));
		processors.add(new CidadeProcessor(em, objects));
		processors.add(new BairroProcessor(em, objects));
		processors.add(new EmpresaProcessor(em, objects));
		processors.add(new FiscalizacaoProcessor(em, objects));
		this.em = em;
	}

	@Override
	public void printLine(CsvGetterInterface csv) {
		System.out.println(csv.toString());
	}

	@Override
	public void processLine(String line, CsvGetterInterface csv) {

		try {
			if (csv.isValid() && ValidateCnpj.isValid(csv.getCnpj())) {
				em.getTransaction().begin();

				for (Processor processor : processors) {
					Long tempoInit = System.nanoTime();

					processor.processLine(line, csv);
					Double lastTime = App.classNameTimeProcessor.get(processor.getClass().getSimpleName());
					if (lastTime == null) {
						lastTime = 0.0;
					}
					App.classNameTimeProcessor.put(processor.getClass().getSimpleName(),
							(System.nanoTime() - tempoInit) / 1_000_000_000.0 + lastTime);
				}

				em.getTransaction().commit();
			}
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		}
	}

}
