package com.faculdade.reader.processor.csvSplitter;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;

public class CsvGettersImpl implements CsvGetterInterface {

	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	
	private String sections[];

	public CsvGettersImpl(String line) {
		this.sections = line.split(";");
	}

	@Override
	public String getUf() {
		return sections[9].toUpperCase();
	}

	@Override
	public String getBairro() {
		return removeSpecialsChars(sections[7].toUpperCase());
	}


	@Override
	public String getCidade() {
		return removeSpecialsChars(sections[8].toUpperCase());
	}

	@Override
	public String getNomeEmpresa() {
		return removeSpecialsChars(sections[3].toUpperCase());
	}

	@Override
	public String getCnpj() {
		return sections[2];
	}

	@Override
	public boolean isValid() {
		return ((sections.length == 10));
	}

	@Override
	public String getAno() {
		return sections[0];
	}

	@Override
	public LocalDate getData() {
		String dateSections[] = sections[1].split("/");
		String year = dateSections[0];
		String month = dateSections[1];
		String date = "01-"+month+"-"+year;
		
		LocalDate dateParsed = LocalDate.parse(date, formatter);
		
		return dateParsed.with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
	}

	@Override
	public String toString() {
		return "csvGettersImpl [sections=" + Arrays.toString(sections) + "]";
	}

	@Override
	public String getLogradouro() {
		return removeSpecialsChars(sections[4].toUpperCase());
	}

	@Override
	public String getCep() {
		return sections[6];
	}

	private String removeSpecialsChars(String string) {
		return string
				.trim()
				.replace("Ç", "C")
				.replace("Ã", "A")
				.replace("Á", "A")
				.replace("Â", "A")
				.replace("Ú", "U")
				.replace("É", "E")
				.replace("Í", "I")
				.replace("Ê", "E")
				.replace(" ", "_");
	}
	
	
}
