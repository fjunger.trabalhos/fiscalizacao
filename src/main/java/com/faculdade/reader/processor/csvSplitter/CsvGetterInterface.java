package com.faculdade.reader.processor.csvSplitter;

import java.time.LocalDate;

public interface CsvGetterInterface {

	public String getUf();
	
	public String getBairro();
	
	public String getCidade();
	
	public String getNomeEmpresa();
	
	public String getCnpj();
	
	public String getAno();
	
	public LocalDate getData();
	
	public String getLogradouro();
	
	public String getCep();
	
	public boolean isValid();
	
	
}
