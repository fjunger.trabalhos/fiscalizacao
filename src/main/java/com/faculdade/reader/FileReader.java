package com.faculdade.reader;

import java.io.BufferedReader;
import java.io.Console;

import static java.time.temporal.ChronoUnit.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

import com.faculdade.reader.processor.Processor;
import com.faculdade.reader.processor.csvSplitter.CsvGettersImpl;

public class FileReader {
	private String path;
	CsvGettersImpl csv;
	
	DateTimeFormatter formatterTerminos = DateTimeFormatter.ofPattern("HH:mm");
	DateTimeFormatter processorTime = DateTimeFormatter.ofPattern("ss:SSS");
	DecimalFormat df = new DecimalFormat("#.##");
	
	private int bigger = 0;
	private int smaller = 300;
	private Double avarage = 0.0;
	int atual;

	public FileReader() {
	}

	public FileReader(String path) {
		this.path = path;
	}

	public void linePrinter(String arquivo, Processor process) throws IOException {
		BufferedReader reader = setReadComponents(arquivo);
		String line = reader.readLine();
		while ((line = reader.readLine()) != null) {
			csv = new CsvGettersImpl(line);
			process.printLine(csv);
		}
		reader.close();
		System.out.println();
	}

	public void read(String path, Processor process) {
		LocalTime init = LocalTime.now();
		int count = 0;
		try {
			BufferedReader reader = setReadComponents(path);
			String line = reader.readLine();
			int i = 1;
			while ((line = reader.readLine()) != null) {
				LocalTime ant = LocalTime.now();
				csv = new CsvGettersImpl(line);
				if (csv.isValid()) {
					process.processLine(line, csv);
				}
				count = setCounters(count, ant);
				i = printLineInfos(init, i, ant);
			}
			reader.close();
		} catch (IOException e) {
			System.out.println("IO Error:");
			e.printStackTrace();
		}
		
		printProcessInfos(init, count);

	}

	private void printProcessInfos(LocalTime init, int count) {
		
		System.out.println("Tempo percorrido: " + tempoPercorrido(init));
		
		System.out.println("Linhas Realmente Processadas: "+ count +"\nProcessamento:\nMédio: " +
				df.format(avarage/count) + " ms\tMáximo: " + bigger + " ms\tMínimo: " + smaller + " ms");
	}

	private int printLineInfos(LocalTime init, int i, LocalTime ant) {
		Long sec = (Long)(init.until(LocalTime.now(), SECONDS));
		Long plus =  (long) ((avarage/i) * (60000 - i))/10000;
		sec = sec + plus;
		System.out.println("Line : " + ++i
				+ " Hora de início: " + init + " Tempo de processamento: ("
				+ (atual + "\tms)")
				+ "\tPrevisão de término: " 
				+ init
				.plusSeconds(sec)
				.format(formatterTerminos) 
				+ " Tempo Percorrido: " 
				+ tempoPercorrido(init)
				);
		return i;
	}

	private String tempoPercorrido(LocalTime init) {
		
		int hora = (int) (init.until(LocalTime.now(), HOURS)); 
		int min = (int) (init.until(LocalTime.now(), MINUTES)); 
		int seg = (int) (init.until(LocalTime.now(), SECONDS)); 
		if(min > 60) {
			min = (int) (init.until(LocalTime.now(), MINUTES)%60);  
		}
		if(seg > 60) {
			seg = (int) init.until(LocalTime.now(), SECONDS)%60;
		}
		
		return (hora + "h" + min + "m" + seg + "s");
	}

	private int setCounters(int count, LocalTime ant) {
		atual = (int) (ant.until(LocalTime.now(), MILLIS));
		if(atual != 0) {
			count ++;
			avarage = (avarage+atual);
			if(atual > bigger) {
				bigger = atual;
			}
			if(smaller > atual) {
				System.out.println(atual);
				smaller = atual;
			}
		}
		return count;
	}

	private BufferedReader setReadComponents(String arquivo) throws FileNotFoundException {
		this.path = arquivo;
		FileInputStream stream = new FileInputStream(path);
		InputStreamReader reader = new InputStreamReader(stream);
		BufferedReader rd = new BufferedReader(reader);
		return rd;
	}

}
