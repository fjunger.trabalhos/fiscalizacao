package com.faculdade.model;

import javax.persistence.*;


@Entity
@Table(name = "tbl_uf",
indexes= {
		@Index(name="searchuf_nome",columnList="nome")
		})
public class Uf  extends AbstractEntity<Long>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "nome", nullable = false, length = 20)
	private String name;
	@Column(name = "sigla", nullable = false, length = 2)
	private String uf;
	
	public Uf(String name, String uf) {
		this.name=name;
		this.uf=uf;
	}
	
	public Uf() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	@Override
	public String toString() {
		return "Uf [name=" + name + ", uf=" + uf + "]";
	}
	
	
	
	
}
