package com.faculdade.model;

import javax.persistence.*;

@Entity
@Table(name = "tbl_cidade",
indexes = {
		@Index(name="searchcidade_by_nome",columnList="nome"),
		@Index(name="searchcidade_by_uf_nome",columnList="uf_fk,nome")
		})
public class Cidade extends AbstractEntity<Long>{
	
	private static final long serialVersionUID = 1L;

	@Column(name = "nome", nullable = false,length = 30)
	private String name;
	
	@OneToOne
	@JoinColumn(name="uf_fk",nullable = false)
	private Uf uf;

	public Cidade() {}

	public Cidade(String nome, Uf uf) {
		this.name = nome;
		this.uf = uf;
	}

	public String getNome() {
		return name;
	}

	public void setNome(String nome) {
		this.name = nome;
	}


	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	@Override
	public String toString() {
		return "Cidade [name=" + name + ", uf=" + uf.toString() + "]";
	}
	
	
	
	
	
}
