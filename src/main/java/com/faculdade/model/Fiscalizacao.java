package com.faculdade.model;

import java.time.LocalDate;

import javax.persistence.*;

@Entity
@Table(name = "tbl_fiscalizacao",
	indexes= {
			@Index(name="searchfiscalizacao_by_cnpj_date",columnList="cnpj")
			})
public class Fiscalizacao extends AbstractEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "razao_social", nullable = false, length = 70)
	private String name;
	@Column(name = "cnpj", nullable = false, length = 18)
	private String cnpj;
	@Column(name = "logradouro", nullable = false)
	private String logradouro;
	@Column(name = "cep", nullable = false, length = 9)
	private String cep;
	@Column(name = "last_fis", nullable = false)
	private LocalDate lastFis;
	@OneToOne
	@JoinColumn(name = "cidade_fk", nullable = false)
	private Cidade cidade;
	@OneToOne
	@JoinColumn(name = "bairro_fk", nullable = false)
	private Bairro bairro;
	@OneToOne
	@JoinColumn(name = "uf_fk", nullable = false)
	private Uf uf;
	@OneToOne
	@JoinColumn(name = "empresa_fk")
	private Empresa empresa;
	
	
	public Fiscalizacao (String name, String cnpj, String logradouro, String cep, LocalDate lastFis, Cidade cidade,
			Bairro bairro, Uf uf, Empresa empresa) {
		this.name = name;
		this.cnpj = cnpj;
		this.logradouro = logradouro;
		this.cep = cep;
		this.lastFis = lastFis;
		this.cidade = cidade;
		this.bairro = bairro;
		this.uf = uf;
		this.empresa = empresa;
	}
	
	public Fiscalizacao() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public LocalDate getLastFis() {
		return lastFis;
	}

	public void setLastFis(LocalDate lastFis) {
		this.lastFis = lastFis;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Fiscalizacao [name=" + name + ", cnpj=" + cnpj + ", empresa=" + empresa + "]";
	}
	
	
	

}
