package com.faculdade.model;

import javax.persistence.*;

@Entity
@Table(name = "tbl_bairro",
indexes = {
		@Index(name="searchbairro_by_name",columnList="nome")
		})
public class Bairro extends AbstractEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "nome", nullable = false, length = 20)
	private String name;
	@OneToOne
    @JoinColumn(name = "uf_fk",nullable = false)
	private Uf uf;
	
	@OneToOne
	@JoinColumn(name = "cidade_fk",nullable = false)
	private Cidade cidade;
	
	public Bairro(String nome, Uf uf, Cidade cidade) {
		this.name = nome;
		this.uf = uf;
		this.cidade = cidade;
	}
	
	public Bairro() {}

	public String getName() {
		return name;
	}

	public void setNome(String nome) {
		this.name = nome;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	@Override
	public String toString() {
		return "Bairro [name=" + name + ", uf=" + uf.toString() + ", cidade=" + cidade.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((uf == null) ? 0 : uf.hashCode());
		return result;
	}
	
	
	
	
	
}
