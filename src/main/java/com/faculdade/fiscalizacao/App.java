package com.faculdade.fiscalizacao;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;

import com.faculdade.connection.JPAUtil;
import com.faculdade.reader.FileReader;
import com.faculdade.reader.processor.*;

/**
 * Hello world!
 *
 */
public class App {

	private static final String path = "arquivoTexto/Empresas - São Paulo.csv";
//	private static final String path = "arquivoTexto/Empresas - São Paulo 200 linhas.csv";
//	private static final String path = "arquivoTexto/Empresas - São Paulo 2000 linhas.csv";
	private static LocalTime begin = LocalTime.now();
	private static DateTimeFormatter formatterTerminos = DateTimeFormatter.ofPattern("HH:mm:ss");
	
	public static Map<String, Double> classNameTimeProcessor = new HashMap<>();

	public static void main(String[] args) throws IOException {

		EntityManager em = JPAUtil.getEntityManager();

		System.out.println(begin);
		System.out.println("Início da Importação");

		FileReader fileReader = new FileReader();
		ProcessImpl lineProcessor = new ProcessImpl(em);
		fileReader.read(path, lineProcessor);

		for(Map.Entry<String, Double> entry : classNameTimeProcessor.entrySet()) {
			System.out.println(entry.getKey() + ": " + entry.getValue());
		}
		
		System.out.println("Término da Importação");
		System.out.println(LocalTime.now().format(formatterTerminos));
		em.close();
		System.exit(0);

	}
}
