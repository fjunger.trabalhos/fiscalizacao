package com.faculdade.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.faculdade.model.Uf;

public class UfDaoImpl extends AbstractDao<Uf, Long> implements UfDao {

	public UfDaoImpl(EntityManager em) {
		super(em);
	}

	@Override
	public boolean exist(Uf uf) {
		try {
			return findByName(uf.getName()) != null;
		} catch (Exception e) {
			return false;
		}
		
	}

	@Override
	public Uf findByName(String nome) {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select u from Uf u ");
		jpql.append(" where ");
		jpql.append(" u.name = ?1");

		return getSingleResult(jpql.toString(), nome);
	}

	@Override
	public List<Uf> getAll() {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select u from Uf u ");
		return createQuery(jpql.toString());
	}

}
