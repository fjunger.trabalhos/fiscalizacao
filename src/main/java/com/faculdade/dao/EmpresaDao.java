package com.faculdade.dao;

import java.util.List;

import com.faculdade.model.Empresa;

public interface EmpresaDao {
	
	public void save(Empresa empresa);

	public void delete(Empresa empresa);

	public void update(Empresa empresa);

	public boolean exist(Empresa empresa);
	
	public Empresa findByCnpj(String cnpj);

	public List<Empresa> getAll();
}
