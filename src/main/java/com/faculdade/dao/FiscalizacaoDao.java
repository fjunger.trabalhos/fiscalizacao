package com.faculdade.dao;

import com.faculdade.model.Fiscalizacao;

public interface FiscalizacaoDao {

	public void save(Fiscalizacao fiscalizacao);

	public void delete(Fiscalizacao fiscalizacao);

	public void update(Fiscalizacao fiscalizacao);

	public boolean exist(Fiscalizacao fiscalizacao);

	public Fiscalizacao findLastByEmpresa(String cnpj);
	
}
