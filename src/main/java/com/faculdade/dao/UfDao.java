package com.faculdade.dao;

import java.util.List;

import com.faculdade.model.Uf;

public interface UfDao {

	public void save(Uf uf);

	public void delete(Uf uf);

	public void update(Uf uf);

	public boolean exist(Uf uf);

	public Uf findByName(String nome);

	public List<Uf> getAll();

}
