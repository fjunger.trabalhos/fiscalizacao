package com.faculdade.dao;


import javax.persistence.EntityManager;

import com.faculdade.model.Empresa;
import com.faculdade.model.Fiscalizacao;

public class FiscalizacaoDaoImpl extends AbstractDao<Fiscalizacao, Long> implements FiscalizacaoDao{

	public FiscalizacaoDaoImpl(EntityManager em) {
		super(em);
	}

	@Override
	public boolean exist(Fiscalizacao fiscalizacao) {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select f from Fiscalizacao f");
		jpql.append(" where ");
		jpql.append(" f.cnpj = ?1 ");
		jpql.append(" and ");
		jpql.append(" f.lastFis = ?2 ");
		
		return getSingleResult(
				jpql.toString(),
				fiscalizacao.getCnpj(),
				fiscalizacao.getLastFis()
				)!=null; 
		
	}

	@Override
	public Fiscalizacao findLastByEmpresa(String cnpj) {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select f from Fiscalizacao f");
		jpql.append(" where ");
		jpql.append(" f.cnpj = ?1");
		jpql.append(" order by ");
		jpql.append(" f.lastFis ");
		jpql.append(" desc ");
		
		
		return getSingleResult(jpql.toString(),cnpj);
	}

}
