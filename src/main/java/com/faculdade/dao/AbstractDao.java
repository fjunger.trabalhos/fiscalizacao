package com.faculdade.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

public abstract class AbstractDao<T, PK extends Serializable> {
	
    @SuppressWarnings("unchecked")
	private final Class<T> entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    @PersistenceContext
    private EntityManager em;
    
    public AbstractDao(EntityManager em){
    	this.em = em;
    }
    
	public void save(T entity) {
			em.persist(entity);
	}
	
	public void delete(T entity) {
		em.remove(entity);
	};
	
	public void update(T entity) {
		em.merge(entity);
	};
	
	protected List<T> createQuery(String jpql, Object... params){
        TypedQuery<T> query = em.createQuery(jpql,entityClass);
        for(int i=0;i< params.length;i++){
            query.setParameter(i+1,params[i]);
        }
        return query.getResultList();
    }
	
	protected T getSingleResult(String jpql, Object... params) {
		 TypedQuery<T> query = em.createQuery(jpql,entityClass);
	        for(int i=0;i< params.length;i++){
	            query.setParameter(i+1,params[i]);
	        }
	        try {
	        	return query.getSingleResult();
	        }catch (Exception e) {
	        	return null;
	        }
	}
	
	protected T getSingleResult(String jpql) {
		 TypedQuery<T> query = em.createQuery(jpql,entityClass);
		 try {
	        	return query.getSingleResult();
	        }catch (Exception e) {
	        	return null;
	        }
	}
	
	protected List<T> createQuery(String jpql){
        TypedQuery<T> query = em.createQuery(jpql,entityClass);
        return query.getResultList();
    }
}
