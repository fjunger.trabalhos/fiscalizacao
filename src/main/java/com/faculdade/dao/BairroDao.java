package com.faculdade.dao;

import java.util.List;

import com.faculdade.model.*;

public interface BairroDao {
	
	public void save(Bairro bairro);

	public void delete(Bairro bairro);

	public void update(Bairro bairro);

	public boolean exist(Cidade cidade,  String nome);
	
	public Bairro findByName(String nome);

	public Bairro findByBairro(String nome, Cidade cidade);

	public List<Bairro> getAll();
}
