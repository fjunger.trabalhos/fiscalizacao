package com.faculdade.dao;

import java.util.List;

import com.faculdade.model.Cidade;
import com.faculdade.model.Uf;

public interface CidadeDao {

	
	public void save(Cidade cidade);

	public void delete(Cidade cidade);

	public void update(Cidade cidade);

	public boolean exist(Uf uf, String nomeCidade);
	
	public Cidade findByUfAndNameCidade(Uf uf, String nomeCidade);

	public Cidade findByName(String nome);

	public List<Cidade> getAll();
	
}
