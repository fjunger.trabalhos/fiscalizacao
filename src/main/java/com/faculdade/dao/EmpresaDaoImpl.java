package com.faculdade.dao;


import java.util.List;

import javax.persistence.EntityManager;

import com.faculdade.model.Empresa;

public class EmpresaDaoImpl extends AbstractDao<Empresa, Long> implements EmpresaDao {

	public EmpresaDaoImpl(EntityManager em) {
		super(em);
	}

	@Override
	public boolean exist(Empresa empresa) {
				
		return findByCnpj(empresa.getCnpj()) != null;
	}


	@Override
	public Empresa findByCnpj(String cnpj) {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select e from Empresa e ");
		jpql.append(" where ");
		jpql.append(" e.cnpj = ?1");
		
		return getSingleResult(jpql.toString(), cnpj);
	}

	@Override
	public List<Empresa> getAll() {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select e from Empresa e ");
		return createQuery(jpql.toString());
	}

}
