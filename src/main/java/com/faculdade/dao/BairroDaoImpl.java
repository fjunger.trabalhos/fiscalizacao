package com.faculdade.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.faculdade.model.Bairro;
import com.faculdade.model.Cidade;
import com.faculdade.model.Uf;

public class BairroDaoImpl extends AbstractDao<Bairro, Long> implements BairroDao{

	public BairroDaoImpl(EntityManager em) {
		super(em);
	}
	
	@Override
	public boolean exist(Cidade cidade, String nome) {
		return findByBairro(nome, cidade) != null;
	}	

	@Override
	public Bairro findByName(String nome) {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select b from Bairro b");
		jpql.append(" where ");
		jpql.append(" b.name = ?1");
		
		return getSingleResult(jpql.toString(), nome);
	}

	@Override
	public Bairro findByBairro(String nome, Cidade cidade) {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select b from Bairro b");
		jpql.append(" where ");
		jpql.append(" b.name = ?1");
		jpql.append(" and ");
		jpql.append(" b.cidade = ?2 ");
		
		return (getSingleResult(
				jpql.toString(),
				nome,
				cidade));
	}

	@Override
	public List<Bairro> getAll() {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select b from Bairro b");
		return createQuery(jpql.toString());
	}



	


}
