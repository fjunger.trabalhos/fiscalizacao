package com.faculdade.dao;


import java.util.List;

import javax.persistence.EntityManager;

import com.faculdade.model.Cidade;
import com.faculdade.model.Uf;

public class CidadeDaoImpl extends AbstractDao<Cidade, Long> implements CidadeDao{

	public CidadeDaoImpl(EntityManager em) {
		super(em);
	}

	@Override
	public boolean exist(Uf uf, String nomeCidade) {
		return findByUfAndNameCidade(uf, nomeCidade) != null;
	}

	@Override
	public Cidade findByName(String nome) {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select c from Cidade c");
		jpql.append(" where ");
		jpql.append(" c.name = ?1");
		
		return getSingleResult(jpql.toString(), nome);
	}

	@Override
	public Cidade findByUfAndNameCidade(Uf uf, String nomeCidade) {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select c from Cidade c");
		jpql.append(" where ");
		jpql.append(" c.name = ?1 ");
		jpql.append(" and ");
		jpql.append(" c.uf = ?2 ");		
		return getSingleResult(
				jpql.toString(),
				nomeCidade,
				uf);
		
	}

	@Override
	public List<Cidade> getAll() {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select c from Cidade c");
		return createQuery(jpql.toString());
	}

}
